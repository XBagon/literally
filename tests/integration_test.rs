#![feature(use_extern_macros)]
#![feature(proc_macro_non_items)]
extern crate literally;
use literally::my_derive;

#[test]
fn try_macro() {
    let x = my_derive!(4$["+","-","*","/"]2);
    println!("{}",x);
}